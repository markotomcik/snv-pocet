const Joi = require('joi')

module.exports = {
  register (req, res, next) {
    const passwordPattern = '^[a-zA-Z0-9]{8,32}$'
    const schema = Joi.object({
      email: Joi.string().email(),
      password: Joi.string().regex(
        new RegExp(passwordPattern)
      )
    })

    const { error, value } = schema.validate(req.body)
    console.log(value)

    if (error) {
      switch (error.details[0].context.key) {
        case 'email':
          res.status(400).send({
            error: 'Zadajte platnú emailovú adresu.'
          })
          break
        case 'password':
          res.status(400).send({
            error: 'Zadajte heslo, ktoré je bezpečné'
          })
          break
        default:
          res.status(400).send({
            error: 'Nesprávne registračné údaje.'
          })
      }
    } else {
      next()
    }
  }
}
