const AuthenticationController = require('./controlers/AuthenticationController')
const SamplingPointController = require('./controlers/SamplingPointController')

const AuthenticationControllerPolicy = require('./policies/AutheticationControllerPolicy')

module.exports = (app) => {
  app.post('/register',
    AuthenticationControllerPolicy.register,
    AuthenticationController.register)

  app.post('/login',
    AuthenticationController.login)

  app.get('/add',
    SamplingPointController.add)

  app.get('/edit/:id',
    SamplingPointController.edit)

  app.get('/:id',
    SamplingPointController.get)
}
