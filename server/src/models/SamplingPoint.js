module.exports = (sequelize, DataTypes) => {
  const SamplingPoint = sequelize.define('SamplingPoint', {
    samplingPointId: {
      type: DataTypes.INTEGER,
      unique: true
    },
    samplingPointName: DataTypes.STRING,
    tested: DataTypes.INTEGER,
    positive: DataTypes.INTEGER,
    waiting: DataTypes.INTEGER
  })

  return SamplingPoint
}
