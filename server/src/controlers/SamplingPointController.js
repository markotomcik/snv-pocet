const { SamplingPoint } = require('../models')

module.exports = {
  async add (req, res) {
    try {
      const samplingPoint = await SamplingPoint.create(req.body)
      res.send(samplingPoint.toJSON())
    } catch (err) {
      res.status(400).send({
        error: 'Toto odberné miesto už bolo pridané.'
      })
    }
  },
  async edit (req, res) {
    try {
      const samplingPoint = await SamplingPoint.update(req.body, {
        where: {
          samplingPointId: req.params.id
        }
      })

      console.log(samplingPoint)
      res.send(req.body)
    } catch (err) {
      res.status(500).send({
        error: 'Chyba pri úprave odberného miesta.'
      })
    }
  },
  async get (req, res) {
    try {
      const samplingPoint = await SamplingPoint.findOne({
        where: {
          samplingPointId: req.params.id
        }
      })

      if (!samplingPoint) {
        return res.status(403).send({
          error: 'Odberné miesto neexistuje.'
        })
      }

      res.send(samplingPoint.toJSON())
    } catch (err) {
      res.status(500).send({
        error: 'Chyba pri hľadaní odberného miesta.'
      })
    }
  }
}
